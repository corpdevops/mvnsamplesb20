package testNGScripts;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import commonUtils.Utility;

public class SampleThreeTest {

	WebDriver driver;
	ExtentReports reports;
	ExtentHtmlReporter htmlReport;
	ExtentTest extentTest;
	
	@BeforeTest
	public void setupExtent() {
		reports = new ExtentReports();
		htmlReport = new ExtentHtmlReporter(System.getProperty("user.dir")
				+ "//reports//ExtentReport.html");
		reports.attachReporter(htmlReport);
	}
	
	@BeforeMethod
	public void setup() {
		  System.setProperty("webdriver.chrome.driver", "F:\\Anandhi\\webdrivers\\chromedriver.exe");
			driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
			driver.manage().window().maximize();
	}
	@Test
	  public void rfwSearchTest() {
			extentTest = reports.createTest("Robot Framework Search Test");
		   	driver.get("https://www.google.com/");
			WebElement searchBox = driver.findElement(By.name("q"));
			searchBox.sendKeys("Robot Framework Tutorial");
			searchBox.submit();
			Assert.assertEquals(driver.getTitle(), "Robot Framework Tutorial - Google Search");
	}
	
	@Test
	  public void restAPISearchTest() {
			extentTest = reports.createTest("Rest API Search Test");
		   	driver.get("https://www.google.com/");
			WebElement searchBox = driver.findElement(By.name("q"));
			searchBox.sendKeys("Rest API Tutorial");
			searchBox.submit();
			Assert.assertEquals(driver.getTitle(), "API Tutorial - Google Search");
	}
	
	@Test(dependsOnMethods = "restAPISearchTest")
	  public void AppiumSearchTest() {
			extentTest = reports.createTest("Appium Search Test");
		   	driver.get("https://www.google.com/");
			WebElement searchBox = driver.findElement(By.name("q"));
			searchBox.sendKeys("Appium Tutorial");
			searchBox.submit();
			Assert.assertEquals(driver.getTitle(), "Appium Tutorial - Google Search");
	}
	
	@AfterMethod
	public void teardown(ITestResult result) throws IOException {
		if(ITestResult.FAILURE == result.getStatus()) {
//			extentTest.fail(result.getThrowable().getMessage());
			String path = Utility.getScreenShotPath(driver);
			extentTest.fail(result.getThrowable().getMessage(),
					 MediaEntityBuilder.createScreenCaptureFromPath(path).build());
		}
		driver.close();
	}
	
	@AfterTest
	public void finishExtent() {
		reports.flush();
	}

}
