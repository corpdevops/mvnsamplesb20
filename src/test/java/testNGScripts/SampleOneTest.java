package testNGScripts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SampleOneTest {
	@Test(groups="featureOne")
	  public void cypressSearchTest() {
		   System.setProperty("webdriver.chrome.driver", "F:\\Anandhi\\webdrivers\\chromedriver.exe");
			WebDriver driver = new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.MILLISECONDS);
			driver.manage().window().maximize();
		  	driver.get("https://www.google.com/");
			WebElement searchBox = driver.findElement(By.name("q"));
			searchBox.sendKeys("Cypress Tutorial");
			searchBox.submit();
			Assert.assertEquals(driver.getTitle(), "Cypress Tutorial - Google Search");
			driver.close();
	  }
	  

}
