package testNGScripts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class ExcelObjTest {
	WebDriver driver;
	
	@BeforeTest
	public void setup() {
//		WebDriverManager.chromedriver().setup();
//		driver = new ChromeDriver();
		driver = new HtmlUnitDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}
	
	 @Test
	  public void verfiyLogin() throws IOException {
		  driver.get("http://the-internet.herokuapp.com/login");
		  System.out.println("Page Title : " + driver.getTitle());
		  driver.findElement(By.id(readFromExcel("txtUsername"))).sendKeys(readDataExcel("username"));
		  driver.findElement(By.id(readFromExcel("txtPassword"))).sendKeys(readDataExcel("password"));
		  driver.findElement(By.cssSelector(readFromExcel("loginBtn"))).click();
		 
		  driver.findElement(By.xpath("//div[@class='radius']"))
//		  boolean isSuccess = driver.findElement(By.cssSelector(readFromExcel("successMsg"))).isDisplayed();
//		  Assert.assertTrue(isSuccess);
	  }
	 public String readFromExcel(String elemName) throws IOException {
		 String elemPath="";
		  String path = System.getProperty("user.dir") +
					 "//src//test//resources//testData//excelData.xlsx";
		  FileInputStream fin = new FileInputStream(path);
		  //.xlsx
		  XSSFWorkbook workbook = new XSSFWorkbook(fin);
		 
		  //.xls ->   HSSFWorkbook
		  XSSFSheet loginSheet = workbook.getSheet("loginRepo");
		  int numRows = loginSheet.getLastRowNum();
		  for(int i=1; i <= numRows; i++) {
			  XSSFRow row = loginSheet.getRow(i);
			  if(row.getCell(0).getStringCellValue().equalsIgnoreCase(elemName)) {
				elemPath = row.getCell(1).getStringCellValue();  
			  }
		  }
		 return elemPath;
	 }
	 
	 public String readDataExcel(String colName) throws IOException {
		 String colValue="";
		  String path = System.getProperty("user.dir") +
					 "//src//test//resources//testData//excelData.xlsx";
		  FileInputStream fin = new FileInputStream(path);
		  //.xlsx
		  XSSFWorkbook workbook = new XSSFWorkbook(fin);
		  //.xls ->   HSSFWorkbook
		  XSSFSheet loginSheet = workbook.getSheet("loginData");
		  int numRows = loginSheet.getLastRowNum();
		  for(int i=1; i <= numRows; i++) {
			  XSSFRow row = loginSheet.getRow(i);
			  if(row.getCell(0).getStringCellValue().equalsIgnoreCase(colName)) {
				  colValue = row.getCell(1).getStringCellValue();  
			  }
		  }
		 return colValue;
	 }
	 @AfterTest
	  public void teardown() {
		  driver.close();
	  }

}
