package testNGScripts;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
//import org.junit.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.opencsv.CSVReader;
import com.opencsv.exceptions.CsvValidationException;

import io.github.bonigarcia.wdm.WebDriverManager;

public class LoginTest {
	WebDriver driver;
	Properties prop;
	
	@BeforeTest
	public void setup() {
	 prop = new Properties();
	 String path = System.getProperty("user.dir") +
			 "//src//test//resources//configFiles//config.properties";
	 FileInputStream fin;
	try {
		fin = new FileInputStream(path);
		prop.load(fin);
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}

	 String strBrowser = prop.getProperty("browser");
		System.out.println("Browser Name : "+ strBrowser);
		if(strBrowser.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		if(strBrowser.equalsIgnoreCase("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.manage().window().maximize();
	}

  @Test(dataProvider="getData")
  public void verfiyLogin(String strUsername, String strPassword) {
	  driver.get(prop.getProperty("url"));
	  driver.findElement(By.id("username")).sendKeys(strUsername);
	  driver.findElement(By.id("password")).sendKeys(strPassword);
	  driver.findElement(By.cssSelector(".radius")).click();
	  boolean isSuccess = driver.findElement(By.cssSelector("div.success")).isDisplayed();
	  Assert.assertTrue(isSuccess);
  }
  
  //username, password
  //user1[] - testuser1, welcome123
  //user2 - testuser2, welcome123
  //user3 - tomsmith, SuperSecretPassword! 
  
//  @DataProvider
//  public Object[][] getData() {
//	return new Object[][]{
//			new Object[] {"testuser1", "welcome123"},
////			new Object[] {"testuser2", "welcome123"},
//			new Object[] {"tomsmith", "SuperSecretPassword!"}
//	};
//  }
  
//  @DataProvider
//  public Object[][] getData() throws CsvValidationException, IOException {
//	  String path = System.getProperty("user.dir") +
//				 "//src//test//resources//testData//loginData.csv";
//	CSVReader reader = new CSVReader(new FileReader(path));
//	String col[];
//	ArrayList<Object> dataList = new ArrayList<Object>();
//	while((col=reader.readNext()) != null) {
//		Object[] record = {col[0], col[1]};
//		dataList.add(record);
//	}
//	reader.close();
//	System.out.println("No. of records : "+ dataList.size());
//	return dataList.toArray(new Object[dataList.size()][]);
// }
  
  @DataProvider
  public String[][] getData() throws IOException, ParseException  {
	  String path = System.getProperty("user.dir") +
				 "//src//test//resources//testData//loginData.json";
	  FileReader reader = new FileReader(path);
	  JSONParser parser = new JSONParser();
	  Object obj = parser.parse(reader);
	  JSONObject jsonObj = (JSONObject)obj;
	  JSONArray userArray = (JSONArray)jsonObj.get("userLogins");
	  String arr[][] = new String[userArray.size()][];
	  for(int i =0; i < userArray.size();i++) {
		JSONObject user = (JSONObject)userArray.get(i);
		String strUser = (String)user.get("username");
		String strPwd = (String)user.get("password");
		String record[] = {strUser, strPwd};
		arr[i]= record;
	  }
	  return arr;
  }
  
  @AfterTest
  public void teardown() {
	  driver.close();
  }
}
